var gulp = require('gulp');
var connect = require('gulp-connect-php');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var plumber = require('gulp-plumber');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var scss = require('gulp-sass');
var cmq = require('gulp-merge-media-queries');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var cache = require('gulp-cache');
var notifier = require('node-notifier');
var notify = require('gulp-notify');
$ = require('gulp-load-plugins')();

var app_path = './app/';
var assets_path = app_path + 'assets/';

// ERROR HANDLE
var onError = function(err) {
  notify.onError({
    title: 'Failure!',
    subtitle: 'Failure!',
    message: 'Error: <%= error.message %>',
    sound: 'Beep'
  })(err);
  this.emit('end');
};

gulp.task('styles', function(done) {
  gulp
    .src([assets_path + 'src/scss/main.scss'])
    .pipe(plumber({ errorHandler: onError }))
    .pipe(sourcemaps.init())
    .pipe(
      scss({
        includePaths: [
          './node_modules/bootstrap-material-design/bower_components/bootstrap-sass/assets/stylesheets/',
          './node_modules/bootstrap-material-design/sass/',
          './node_modules/bootstrap/scss/'
        ],
        errLogToConsole: true
      })
    )
    .pipe(cmq({ log: true }))
    .pipe(concat('app-main.css'))
    .pipe(gulp.dest(assets_path + 'dist/css'))
    .pipe(
      scss({
        outputStyle: 'compact'
      })
    )
    .pipe(
      rename({
        suffix: '.min'
      })
    )
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(assets_path + 'dist/css'))
    .pipe(reload({ stream: true }));
  done();
});

gulp.task('scripts', function(done) {
  gulp
    .src([assets_path + 'src/js/app-main.js'])
    .pipe(plumber({ errorHandler: onError }))
    .pipe(concat('app-main.js'))
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(gulp.dest(assets_path + 'dist/js'))
    .pipe(
      rename({
        suffix: '.min'
      })
    )
    .pipe(uglify())
    .pipe(gulp.dest(assets_path + 'dist/js'))
    .pipe(reload({ stream: true }));
  done();
});

gulp.task('fonts', function(done) {
  gulp
    .src([assets_path + 'src/fonts/*'])
    .pipe(plumber({ errorHandler: onError }))
    .pipe(gulp.dest(assets_path + 'dist/fonts'))
    .pipe(reload({ stream: true }));
  done();
});

gulp.task('plugins-styles', function() {
  return gulp
    .src([
      './node_modules/bootstrap/dist/css/bootstrap.min.css',
      './node_modules/snackbarjs/dist/snackbar.min.css',
      './node_modules/bootstrap-material-design/dist/css/bootstrap-material-design.min.css'
    ])
    .pipe(gulp.dest(assets_path + '/dist/css'));
});

gulp.task('plugins-scripts', function() {
  return gulp
    .src([
      './node_modules/bootstrap-material-design/bower_components/jquery/dist/jquery.min.js',
      './node_modules/bootstrap-material-design/bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',
      './node_modules/bootstrap-material-design/dist/js/material.min.js',
      './node_modules/jquery-validation/dist/jquery.validate.js',
      './node_modules/snackbarjs/dist/snackbar.min.js'
    ])
    .pipe(concat('app-plugins.js'))
    .pipe(gulp.dest(assets_path + '/dist/js'));
});

gulp.task('plugins', gulp.series('plugins-styles', 'plugins-scripts'));

gulp.task('dev', function(done) {
  connect.server({ base: app_path, port: 8010 }, function() {
    browserSync.init({
      proxy: 'localhost:8010',
      port: 4000,
      files: [assets_path + 'dist/**/*', app_path + '**/*.php'],
      ghostMode: false,
      online: false,
      open: false
    });
  });

  gulp.watch(assets_path + 'src/js/**/*.js', gulp.parallel('scripts'));
  gulp.watch(assets_path + 'src/scss/**/*.scss', gulp.parallel('styles'));
  gulp.watch(assets_path + 'src/fonts/*', gulp.parallel('fonts'));

  done();
});

gulp.task('notify', function(done) {
  notifier.notify({
    title: 'Production Build',
    subtitle: 'Success!',
    message: 'Default task complete!',
    sound: 'Pop'
  });
  done();
});

// Default task
gulp.task(
  'default',
  gulp.series('styles', 'scripts', 'fonts', 'plugins', 'notify')
);
