<?php

include_once "include/head.html";

// Load DB config file
require_once "include/db.php";

$row = array(
	'contact_name' => '',
	'contact_company' => '',
	'contact_address' => '',
	'contact_phone' => '',
	'contact_email' => '',
	'contact_notes' => ''
);

if ( isset($_REQUEST['id']) ) {

	// Select contact data
	$sql = "SELECT * FROM `contact_data` WHERE `ID` = " . $_REQUEST['id'];
	$select_query = mysqli_query($conn, $sql);
  	$row = mysqli_fetch_array($select_query);

}

?>

<body>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">

				<div class="well bs-component">

					<div class="row">
						<div class="col-xs-6">
							<h4 class="text-left">Edit</h4>
						</div>

						<div class="col-xs-6 text-right">
							<a class="btn btn-raised btn-primary header-option-button" href="index.php"><i class="material-icons">&#xE5C4;</i> Back</a>
						</div>
					</div>

					<hr />

					<div class="row">
						<div class="col-xs-12">
							<form action="" method="post" class="contact-form">

								<input id="form-type" type="hidden" name="form-type" value="edit">
								<input type="hidden" name="contactID" value="<?=$_REQUEST['id'];?>">

								<div class="validation-errors"></div>

								<div class="form-group label-floating required">
								  	<label for="contact_name" class="control-label">Name</label>
								  	<input type="text" name="contact_name" id="contact_name" value="<?=$row['contact_name'];?>" class="form-control" />
								</div>

								<div class="form-group label-floating">
								  	<label for="contact_company" class="control-label">Company</label>
								  	<input type="text" name="contact_company" id="contact_company" value="<?=$row['contact_company'];?>" class="form-control" />
								</div>

								<div class="form-group label-floating">
								  	<label for="contact_address" class="control-label">Address</label>
								  	<input type="text" name="contact_address" id="contact_address" value="<?=$row['contact_address'];?>" class="form-control" />
								</div>

								<div class="form-group label-floating required">
								  	<label for="contact_phone" class="control-label">Phone Number</label>
								  	<input type="tel" name="contact_phone" id="contact_phone" value="<?=$row['contact_phone'];?>" class="form-control" />
								</div>

								<div class="form-group label-floating">
								  	<label for="contact_email" class="control-label">Email Address</label>
								  	<input type="email" name="contact_email" id="contact_email" value="<?=$row['contact_email'];?>" class="form-control" />
								</div>

								<div class="form-group label-floating">
								  	<label for="contact_notes" class="control-label">Notes</label>
								  	<textarea class="form-control" maxlength="80" name="contact_notes" id="contact_notes"><?=$row['contact_notes'];?></textarea>
								  	<p class="help-block">Maximum length of 80 characters</p>
								</div>

								<button id="submitForm" type="submit" class="btn btn-raised btn-info btn-block"><i class="material-icons">&#xE254;</i> Edit contact</button>

							</form>
						</div>
					</div>


				</div>

			</div>
		</div>
	</div>

</body>
</html>
