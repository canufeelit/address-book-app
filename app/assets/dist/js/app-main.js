$(document).ready(function() {
  //================================
  // Ajax Form Request on submit
  //================================
  var formAJAX = function(url) {
    var ajaxurl = 'include/' + url + '.php';
    var queryData = $('form').serialize();

    $.ajax({
      type: 'POST',
      data: queryData,
      dataType: 'json',
      async: false,
      url: ajaxurl
    }).always(function(ajax_result) {
      if (ajax_result.error === 0) {
        if (ajax_result.snackbar) {
          // SnackbarJS Options
          var options = {
            content: ajax_result.snackbar, // text of the snackbar
            style: 'snack', // add a custom class to your snackbar
            timeout: 3000, // time in milliseconds after the snackbar autohides, 0 is disabled
            htmlAllowed: true, // allows HTML as content value
            onClose: function() {
              // callback called when the snackbar gets closed.

              // Redirect if successful
              if (ajax_result.redirect) {
                location.href = ajax_result.redirect;
              }
            }
          };

          // Init SnackbarJS
          $.snackbar(options);
        }
      } else {
        $('.validation-errors').show();
        $(ajax_result.message).each(function(index, el) {
          $('.validation-errors').append('<span>' + el + '</span>');
          $('.contact-form')
            .find('.required')
            .addClass('has-error');
        });
      }
    });
  };

  //=========================
  // Form validation
  //=========================
  $.validator.setDefaults({
    errorClass: 'text-danger error-block',
    errorElement: 'span'
  });

  var validator = $('.contact-form').validate({
    ignore: null,
    ignoreTitle: true,
    rules: {
      contact_name: 'required',
      contact_email: {
        required: true,
        email: true
      },
      contact_phone: {
        required: true,
        digits: true
      }
    },
    invalidHandler: function(form, validator) {
      if (!validator.numberOfInvalids()) return;

      $('html, body').animate(
        {
          scrollTop:
            $(validator.errorList[0].element.parentElement).offset().top - 150
        },
        1000
      );
    },
    onkeyup: function(element, errorClass, validClass) {
      // Enable the submit button if disabled
      $('#submitForm').prop('disabled', false);
    }
  });

  //=======================================================
  // Events that occur when the submit button is clicked
  //=======================================================
  $('#submitForm').on('click', function() {
    // Disable the button
    $(this).prop('disabled', true);

    // Validate the whole form on click
    var validationPassed = validator.form();

    // Proceed with backend checks and validations
    if (validationPassed) {
      var form_type = $('.contact-form')
        .find('#form-type')
        .val();

      // Call the formAJAX function
      formAJAX(form_type);
    }

    // Clear errors
    $('.validation-errors').html('');
    $('.validation-errors').hide();
  });
});

$(window).load(function() {
  // Initialize Material Design for Bootstrap
  $.material.init();
});
