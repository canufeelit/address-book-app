<?php

include_once "include/head.html";

?>

<body>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">

				<div class="well bs-component">

					<div class="row">
						<div class="col-xs-6">
							<h4 class="text-left">Add</h4>
						</div>

						<div class="col-xs-6 text-right">
							<a class="btn btn-raised btn-primary header-option-button" href="index.php"><i class="material-icons">&#xE5C4;</i> Back</a>
						</div>
					</div>

					<hr />

					<div class="row">
						<div class="col-xs-12">
							<form action="" method="post" class="contact-form">

								<input id="form-type" type="hidden" name="form-type" value="add">

								<div class="validation-errors"></div>

								<div class="form-group label-floating required">
								  	<label for="contact_name" class="control-label">Name</label>
								  	<input type="text" name="contact_name" id="contact_name" class="form-control" />
								</div>

								<div class="form-group label-floating">
								  	<label for="contact_company" class="control-label">Company</label>
								  	<input type="text" name="contact_company" id="contact_company" class="form-control" />
								</div>

								<div class="form-group label-floating">
								  	<label for="contact_address" class="control-label">Address</label>
								  	<input type="text" name="contact_address" id="contact_address" class="form-control" />
								</div>

								<div class="form-group label-floating required">
								  	<label for="contact_phone" class="control-label">Phone Number</label>
								  	<input type="tel" name="contact_phone" id="contact_phone" class="form-control" />
								</div>

								<div class="form-group label-floating">
								  	<label for="contact_email" class="control-label">Email Address</label>
								  	<input type="email" name="contact_email" id="contact_email" class="form-control" />
								</div>

								<div class="form-group label-floating">
								  	<label for="contact_notes" class="control-label">Notes</label>
								  	<textarea class="form-control" maxlength="80" name="contact_notes" id="contact_notes"></textarea>
								  	<p class="help-block">Maximum length of 80 characters</p>
								</div>

								<button id="submitForm" type="submit" class="btn btn-raised btn-primary btn-block"><i class="material-icons">&#xE145;</i> Add contact</button>

							</form>
						</div>
					</div>


				</div>

			</div>
		</div>
	</div>

</body>
</html>
