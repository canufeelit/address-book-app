<?php

$ajax = array();
$ajax['error'] = 0;
$ajax['message'] = '';

if ( empty($_POST['contact_name']) ) {

	$ajax['error'] = 1;
	$ajax['message'][] = "Name field is required!";
}

if ( empty($_POST['contact_phone']) ) {

	$ajax['error'] = 1;
	$ajax['message'][] = "Phone field is required!";
}

if ( $ajax['error'] ) {

	echo json_encode($ajax);
	exit;
}

$ID = $_POST['contactID'];
$contact_name = $_POST['contact_name'];
$contact_company = $_POST['contact_company'];
$contact_address = $_POST['contact_address'];
$contact_phone = $_POST['contact_phone'];
$contact_email = $_POST['contact_email'];
$contact_notes = $_POST['contact_notes'];

// Load DB config file
require_once("db.php");

// Update contact data
$sql = "UPDATE `contact_data` SET `contact_name` = '$contact_name', `contact_company` = '$contact_company', `contact_address` = '$contact_address', `contact_phone` = '$contact_phone', `contact_email` = '$contact_email', `contact_notes` = '$contact_notes', `updated` = CURRENT_TIMESTAMP WHERE `contact_data`.`ID` = " . $ID;
$insert_query = mysqli_query($conn, $sql);

// Close the connection to DB
mysqli_close($conn);

if ( !$insert_query ) {

	$ajax['message'][] = "Error saving changes to DB.";
	$ajax['snackbar'] = "There was a problem editing this contact!";

	echo json_encode($ajax);
	exit;
}

$ajax['redirect'] = 'index.php';
$ajax['snackbar'] = "Contact edited successfully!";

echo json_encode($ajax);
exit;
