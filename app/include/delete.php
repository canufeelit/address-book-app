<?php

$ajax = array();
$ajax['error'] = 0;
$ajax['message'] = '';

// Load DB config file
require_once("db.php");

if ( isset($_REQUEST['contactID']) ) {

	// Delete contact data
	$sql = "DELETE FROM `contact_data` WHERE `contact_data`.`ID` = " . $_REQUEST['contactID'];
	$delete_query = mysqli_query($conn, $sql);
}

// Close the connection to DB
mysqli_close($conn);

if ( !$delete_query ) {

	$ajax['message'][] = "Error saving changes to DB.";
	$ajax['snackbar'] = "There was a problem deleting this contact!";

	echo json_encode($ajax);
	exit;
}

$ajax['redirect'] = 'index.php';
$ajax['snackbar'] = "Contact deleted successfully!";

echo json_encode($ajax);
exit;
